const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
let counter = 0
const root = document.getElementById('root')
let ul = document.createElement('ul')
for(let book of books){
    counter++
    let li = document.createElement('li')
    if(book.author){
        li.textContent = book.author
    }
    else{
        console.log(`In the book №${counter} the author is not exist`)
        continue
    }
    if(book.name){
        li.textContent += ", " + book.name
    }
    else{
        console.log(`In the book №${counter} the name is not exist`)
        li.textContent = ''
        continue
    }
    if(book.price){
        li.textContent += ", " + book.price
        ul.append(li)
    }
    else{
        console.log(`In the book №${counter} the price is not exist`)
        li.textContent=''
        continue
    }
}
root.append(ul)
